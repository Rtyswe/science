from scipy.special import *
import math
import numpy as np
from scipy.integrate import quad


def f(x, nu, a, R):
    return math.exp(-(x/a)**2) * j0(nu * x / R) * x


def Fn(nu, fi, P, a, R):
    v, err = quad(f, 0, 1, args = (nu, a, R))
    return 2 * fi * P * v / (R**2 * j0(nu)**2 * math.pi * a**2)


def Lambda(nu, R, g, q):
    return ((nu / R) ** 2) * g + q


def Temperature(time, radius):
    amount = 0
    for j in range(0, len(nuN)):
        lambd = Lambda(nuN[j], R, g, q)
        fn = Fn(nuN[j], fi, P, a, R)
        amount += fn * (1 - math.exp(-lambd * time)) * j0(nuN[j] * radius / R) / lambd
    return amount


with open("../k.txt") as file:
    nuN = [float(row.strip()) for row in file]

fi = 0.1
R = 3
T = 10
P = 60
a = 0.3
alpha = 0.005
c = 1.65
L = 1
k = 0.59

q = 2 * alpha / (c * L)
g = k / c
Ir = P / (math.pi * a ** 2)

K_0 = 50
I_0 = 100
I_ar = [I_0, 2*I_0]
K_ar = [K_0, 4*K_0]
U_0 = []

for index in range(2):
    I = I_ar[index]
    K = K_ar[index]

    hr = R / I
    ht = T / K

    r = []
    for i in range(I + 1):
        r.append(i * hr)
    temp1 = np.zeros(I + 1)
    percent = 0
    for k in range(1, K + 1):
        A = []
        B = []
        C = []
        D = []
        # считаем коэффициенты A, B, C и D
        temp = -4 * g * ht / hr ** 2
        A.append(temp)
        temp = 1 + 4 * g * ht / hr ** 2 + q * ht
        B.append(temp)
        C.append(0.0)
        temp = temp1[0]
        if 0 < a:
            temp += fi * ht * Ir
        D.append(temp)
        for ind in range(1, I):
            temp = -g * ht * (2 * ind + 1) / (2 * ind * hr ** 2)
            A.append(temp)
            temp = 1 + 2 * g * ht / hr ** 2 + q * ht
            B.append(temp)
            temp = -g * ht * (2 * ind - 1) / (2 * ind * hr ** 2)
            C.append(temp)
            temp = temp1[ind]
            if ind * hr < a:
                temp += fi * ht * Ir
            D.append(temp)
        A.append(0.0)
        temp = 1 + 2 * g * ht / hr ** 2 + q * ht
        B.append(temp)
        temp = -2 * g * ht / hr ** 2
        C.append(temp)
        temp = temp1[I]
        if R < a:
            temp += fi * ht * Ir
        D.append(temp)

        alphas = []
        betas = []

        # считаем коэффициенты alphas и betas
        temp = -A[0] / B[0]
        alphas.append(temp)
        temp = D[0] / B[0]
        betas.append(temp)
        for ind in range(1, I):
            temp = -A[ind] / (B[ind] + C[ind] * alphas[ind - 1])
            alphas.append(temp)
            temp = (D[ind] - C[ind] * betas[ind - 1]) / (B[ind] + C[ind] * alphas[ind - 1])
            betas.append(temp)

        # считаем значения
        temp1[I] = (D[I] - C[I] * betas[I - 1]) / (B[I] + C[I] * alphas[I - 1])
        for i in range(I - 1, -1, -1):
            temp1[i] = alphas[i] * temp1[i + 1] + betas[i]

        if int(k / K * 100) > percent:
            percent = int(k / K * 100)
            print(str(percent) + "%")

    U_0.append(temp1[0])

U_anal = 6.956404454286235
print("I = ", I_ar[0], end=" ")
print("K = ", K_ar[0], end=" ")
print("E(ht, hr) = ", U_anal - U_0[0], end=" ")
print("E(ht/4, hr/2) = ", U_anal - U_0[1], end=" ")
print("Del'ta = ", (U_anal - U_0[0]) / (U_anal - U_0[1]), end=" ")
