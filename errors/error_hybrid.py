from scipy.special import *
import math
import numpy as np


def Fn(nu):
    return 2 * fi * P * j1(nu * a / R) / (math.pi * a * nu * R * j0(nu) ** 2)


def F0():
    return fi * P / R ** 2 / math.pi


def Lambda(nu):
    return ((nu / R) ** 2) * g + q


def Temperature(time, radius):
    amount = F0() * (1 - math.exp(-q * time)) / q
    for j in range(1, len(nuN)):
        lambd = Lambda(nuN[j])
        fn = Fn(nuN[j])
        amount += fn * (1 - math.exp(-lambd * time)) * j0(nuN[j] * radius / R) / lambd
    return amount


with open("../k.txt") as file:
    nuN = [float(row.strip()) for row in file]

fi = 0.1
R = 3
T = 10
P = 60
a = 0.3
alpha = 0.005
c = 1.65
L = 1
k = 0.59

q = 2 * alpha / (c * L)
g = k / c
Ir = P / (math.pi * a ** 2)

K_0 = 500
I_0 = 50
I_ar = [I_0, 4*I_0]
K_ar = [K_0, 8*K_0]

U_0 = []

for index in range(2):
    K = K_ar[index]
    I = I_ar[index]
    ht = T / K
    hr = R / I

    r = []
    for i in range(I + 1):
        r.append(i * hr)
    temp1 = np.zeros(I + 1)

    percent = 0
    for i in range(1, K + 1):
        temp2 = np.zeros(I+1)
        if i % 2 == 0:
            # вычисляем 0-й узел по r
            temp = 4 * g * ht * (temp1[1] - temp1[0]) / (hr ** 2) + (1 - q * ht) * temp1[0] + ht * fi * Ir
            temp2[0] = temp

            # вычисляем последний узел по r
            temp = 2 * g * ht * (temp1[I - 1] - temp1[I]) / (hr ** 2) + (1 - q * ht) * temp1[I]
            temp2[I] = temp

            # считаем узлы с помощью явной схемы
            for j in range(2, I - 1, 2):
                temp = g * ht * ((temp1[j + 1] - 2 * temp1[j] + temp1[j - 1]) / (hr ** 2) +
                                 (temp1[j + 1] - temp1[j - 1]) / (2 * j * hr ** 2)) + (1 - q * ht) * temp1[j]
                if j * hr < a:
                    temp += ht * fi * Ir
                temp2[j] = temp

            # считаем узлы с помощью неявной схемы
            for j in range(1, I, 2):
                temp = g * ht * ((temp2[j + 1] + temp2[j - 1]) / hr ** 2 +
                                 (temp2[j + 1] - temp2[j - 1]) / (2 * j * hr ** 2)) + temp1[j]
                if j * hr < a:
                    temp += ht * fi * Ir
                temp /= 1 + 2 * g * ht / hr ** 2 + q * ht
                temp2[j] = temp

        else:
            # считаем узлы с помощью явной схемы
            for j in range(1, I, 2):
                temp = g * ht * ((temp1[j + 1] - 2 * temp1[j] + temp1[j - 1]) / (hr ** 2) +
                                 (temp1[j + 1] - temp1[j - 1]) / (2 * j * hr ** 2)) + (1 - q * ht) * temp1[j]
                if j * hr < a:
                    temp += ht * fi * Ir
                temp2[j] = temp

            # вычисляем 0-й узел неявно
            temp = 4 * g * ht * temp2[1] / hr ** 2 + ht * fi * Ir + temp1[0]
            temp /= 1 + 4 * g * ht / hr ** 2 + q * ht
            temp2[0] = temp

            # вычисляем последний узел неявно
            temp = 2 * g * ht * temp2[I - 1] / hr ** 2 + temp1[I]
            temp /= 1 + 2 * g * ht / hr ** 2 + q * ht
            temp2[I] = temp

            # считаем узлы с помощью неявной схемы
            for j in range(2, I - 1, 2):
                temp = g * ht * ((temp2[j + 1] + temp2[j - 1]) / hr ** 2 +
                                 (temp2[j + 1] - temp2[j - 1]) / (2 * j * hr ** 2)) + temp1[j]
                if j * hr < a:
                    temp += ht * fi * Ir
                temp /= 1 + 2 * g * ht / hr ** 2 + q * ht
                temp2[j] = temp

        temp1 = temp2
        if int(i / K * 100) > percent:
            percent = int(i / K * 100)
            print(str(percent) + "%")

    U_0.append(temp1[0])

U_anal = Temperature(T, 0)
print("I = ", I_ar[0])
print("K = ", K_ar[0])
print("E(ht, hr) = ", U_anal - U_0[0])
print("E(ht/4, hr/2) = ", U_anal - U_0[1])
print("Del'ta = ", (U_anal - U_0[0]) / (U_anal - U_0[1]))
