import math
import matplotlib.pyplot as plt
import numpy as np

x_weird = 8.5 * 10**(-6)
n_0 = 4
const_lambda = 10.6 * 10**(-4)
a_l = 6 * 10**(-6)
a_n = 2.67 * 10**(-4)
l_0 = 1


def fi(temp_value):
    n = n_0 + temp_value * a_l
    l = l_0 * (1 + a_l * temp_value)
    k_v_ch = 2 * np.pi * n / const_lambda
    eps = k_v_ch * l
    return 4 * x_weird * k_v_ch * (1 + n**2) / (4 * n**2 + (1 - n**2)**2 * np.sin(eps)**2)


R = 3
T = 2000
P = 60
a = 0.3
alpha = 0.005
c = 1.65
L = 1
const_k = 0.59
I = 1000
K = 1000

q = 2 * alpha / (c * L)
g = const_k / c
Ir = P / (math.pi * a ** 2)

hr = R / I
ht = T / K

r = []
for i in range(I + 1):
    r.append(i * hr)
temp1 = np.zeros(I + 1)

percent = 0
A = []
B = []
C = []
temp = -4 * g * ht / hr ** 2
A.append(temp)
temp = 1 + 4 * g * ht / hr ** 2 + q * ht
B.append(temp)
C.append(0.0)
for ind in range(1, I):
    temp = -g * ht * (2 * ind + 1) / (2 * ind * hr ** 2)
    A.append(temp)
    temp = 1 + 2 * g * ht / hr ** 2 + q * ht
    B.append(temp)
    temp = -g * ht * (2 * ind - 1) / (2 * ind * hr ** 2)
    C.append(temp)
A.append(0.0)
temp = 1 + 2 * g * ht / hr ** 2 + q * ht
B.append(temp)
temp = -2 * g * ht / hr ** 2
C.append(temp)

percent = 0
for k in range(1, K + 1):
    D = []

    # считаем коэффициенты D
    temp = temp1[0]
    if 0 < a:
        temp += fi(temp1[0]) * ht * Ir
    D.append(temp)

    for ind in range(1, I):
        temp = temp1[ind]
        if ind * hr < a:
            temp += fi(temp1[ind]) * ht * Ir
        D.append(temp)
    temp = temp1[I]
    if R < a:
        temp += fi(temp1[I]) * ht * Ir
    D.append(temp)

    alphas = []
    betas = []

    # считаем коэффициенты alphas и betas
    temp = -A[0] / B[0]
    alphas.append(temp)
    temp = D[0] / B[0]
    betas.append(temp)
    for ind in range(1, I):
        temp = -A[ind] / (B[ind] + C[ind] * alphas[ind - 1])
        alphas.append(temp)
        temp = (D[ind] - C[ind] * betas[ind - 1]) / (B[ind] + C[ind] * alphas[ind - 1])
        betas.append(temp)

    # считаем значения
    temp1[I] = (D[I] - C[I] * betas[I - 1]) / (B[I] + C[I] * alphas[I - 1])
    for i in range(I - 1, -1, -1):
        temp1[i] = alphas[i] * temp1[i + 1] + betas[i]

    if int(k / K * 100) > percent:
        percent = int(k / K * 100)
        print(str(percent) + "%")

plt.xlabel("r, радиус")
plt.ylabel("U, температура")
plt.plot(r, temp1, label="Неявн.: t = " + str(T))
plt.legend()
plt.show()
