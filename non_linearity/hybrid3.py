import math
import matplotlib.pyplot as plt
import numpy as np


x_weird = 8.5 * 10**(-6)
n_0 = 4
const_lambda = 10.6 * 10**(-4)
a_l = 6 * 10**(-6)
a_n = 2.67 * 10**(-4)
l_0 = 1


def fi(temp_value):
    n = n_0 + temp_value * a_l
    l = l_0 * (1 + a_l * temp_value)
    k_v_ch = 2 * np.pi * n / const_lambda
    eps = k_v_ch * l
    return 4 * x_weird * k_v_ch * (1 + n**2) / (4 * n**2 + (1 - n**2)**2 * np.sin(eps)**2)


R = 3
T = 100
P = 60
a = 0.3
alpha = 0.005
c = 1.65
L = 1
k = 0.59
I = 1000
K = 10000

q = 2 * alpha / (c * L)
g = k / c
Ir = P / (math.pi * a ** 2)

hr = R / I
ht = T / K

r = []
for i in range(I + 1):
    r.append(i * hr)
temp1 = np.zeros(I + 1)

percent = 0
for i in range(1, K + 1):
    temp2 = np.zeros(I + 1)

    if i % 2 == 0:
        # вычисляем 0-й узел по r
        temp = 4 * g * ht * (temp1[1] - temp1[0]) / (hr ** 2) + (1 - q * ht) * temp1[0]
        if 0 * hr < a:
            temp += ht * fi(temp1[0]) * Ir
        temp2[0] = temp

        # вычисляем последний узел по r
        temp = 2 * g * ht * (temp1[I - 1] - temp1[I]) / (hr ** 2) + (1 - q * ht) * temp1[I]
        if I * hr < a:
            temp += ht * fi(temp1[I]) * Ir
        temp2[I] = temp

        # считаем узлы с помощью явной схемы
        for j in range(2, I - 1, 2):
            temp = g * ht * ((temp1[j + 1] - 2 * temp1[j] + temp1[j - 1]) / (hr ** 2) +
                             (temp1[j + 1] - temp1[j - 1]) / (2 * j * hr ** 2)) + (1 - q * ht) * temp1[j]
            if j * hr < a:
                temp += ht * fi(temp1[j]) * Ir
            temp2[j] = temp

        # считаем узлы с помощью неявной схемы
        for j in range(1, I, 2):
            temp = g * ht * ((temp2[j + 1] + temp2[j - 1]) / hr ** 2 +
                             (temp2[j + 1] - temp2[j - 1]) / (2 * j * hr ** 2)) + temp1[j]
            if j * hr < a:
                temp += ht * fi(temp1[j]) * Ir
            temp /= 1 + 2 * g * ht / hr ** 2 + q * ht
            temp2[j] = temp

    else:
        # считаем узлы с помощью явной схемы
        for j in range(1, I, 2):
            temp = g * ht * ((temp1[j + 1] - 2 * temp1[j] + temp1[j - 1]) / (hr ** 2) +
                             (temp1[j + 1] - temp1[j - 1]) / (2 * j * hr ** 2)) + (1 - q * ht) * temp1[j]
            if j * hr < a:
                temp += ht * fi(temp1[j]) * Ir
            temp2[j] = temp

        # вычисляем 0-й узел неявно
        temp = 4 * g * ht * temp2[1] / hr ** 2 + temp1[0]
        if 0 * hr < a:
            temp += ht * fi(temp1[0]) * Ir
        temp /= 1 + 4 * g * ht / hr ** 2 + q * ht
        temp2[0] = temp

        # вычисляем последний узел неявно
        temp = 2 * g * ht * temp2[I - 1] / hr ** 2 + temp1[I]
        if I * hr < a:
            temp += ht * fi(temp1[I]) * Ir
        temp /= 1 + 2 * g * ht / hr ** 2 + q * ht
        temp2[I] = temp

        # считаем узлы с помощью неявной схемы
        for j in range(2, I - 1, 2):
            temp = g * ht * ((temp2[j + 1] + temp2[j - 1]) / hr ** 2 +
                             (temp2[j + 1] - temp2[j - 1]) / (2 * j * hr ** 2)) + temp1[j]
            if j * hr < a:
                temp += ht * fi(temp1[j]) * Ir
            temp /= 1 + 2 * g * ht / hr ** 2 + q * ht
            temp2[j] = temp

    temp1 = temp2
    if int(i / K * 100) > percent:
        percent = int(i / K * 100)
        print(str(percent) + "%")

plt.xlabel("r, радиус")
plt.ylabel("U, температура")
plt.plot(r, temp1, label="гибридная3: t = " + str(T))
plt.legend()
plt.show()
