import math
import numpy as np
import time


def func():
    R = params["R"]
    T = params["T"]
    P = params["P"]
    a = params["a"]
    alpha = params["alpha"]
    c = params["c"]
    L = params["L"]
    k = params["k"]
    I = params["I"]
    K = params["K"]
    x_weird = params["x_weird"]
    const_lambda = params["lambda"]
    n_0 = params["n_0"]
    a_n = params["a_n"]
    l_0 = params["l_0"]
    a_l = params["a_l"]

    Ir = P / (math.pi * a ** 2)
    q = 2 * alpha / (c * L)
    g = k / c

    hr = R / I
    ht = T / K

    r = []
    for i in range(I + 1):
        r.append(i * hr)
    temp1 = np.zeros(I + 1)

    percent = 0
    for k in range(1, K + 1):
        temp2 = np.zeros(I + 1)

        # вычисляем 0-й узел по r
        temp2[0] = 4 * g * ht * (temp1[1] - temp1[0]) / (hr ** 2) + (1 - q * ht) * temp1[0] + ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[0]) * Ir

        # считаем узлы с помощью явной схемы
        for j in range(1, I):
            temp = g * ht * ((temp1[j + 1] - 2 * temp1[j] + temp1[j - 1]) / (hr ** 2) +
                             (temp1[j + 1] - temp1[j - 1]) / (2 * j * hr ** 2)) + (1 - q * ht) * temp1[j]
            if j * hr < a:
                temp += ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[j]) * Ir
            temp2[j] = temp

        # вычисляем последний узел по r
        temp2[I] = 2 * g * ht * (temp1[I - 1] - temp1[I]) / (hr ** 2) + (1 - q * ht) * temp1[I]

        temp1 = temp2

        if int(k / K * 100) > percent:
            percent = int(k / K * 100)
            print(str(percent) + " %")

    return temp1[0]


def fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp_value):
    n = n_0 + temp_value * a_l
    l = l_0 * (1 + a_l * temp_value)
    k_v_ch = 2 * np.pi * n / const_lambda
    eps = k_v_ch * l
    return 4 * x_weird * k_v_ch * (1 + n ** 2) / (4 * n ** 2 + (1 - n ** 2) ** 2 * np.sin(eps) ** 2)


exact_solution = 11.459007533277603
params = {"R": 3, "T": 10, "P": 60, "a": 0.3, "alpha": 0.005, "c": 1.65, "L": 1, "k": 0.59,
          "x_weird": 8.5 * 10e-6, "lambda": 10.6 * 10e-4, "n_0": 4, "a_n": 4 * 10e-4, "l_0": 1, "a_l": 4 * 10e-6,
          "I": 48, "K": 3000}


value_time = time.time()
sol = func()
value_time = time.time() - value_time
print("sol = " + str(sol))
print("eps  = " + str(exact_solution - sol))
print("time = " + str(value_time))
print("I    = " + str(params["I"]))
print("K    = " + str(params["K"]))
