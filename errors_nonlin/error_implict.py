import math
import numpy as np
import time


def func():
    R = params["R"]
    T = params["T"]
    P = params["P"]
    a = params["a"]
    alpha = params["alpha"]
    c = params["c"]
    L = params["L"]
    k = params["k"]
    I = params["I"]
    K = params["K"]
    x_weird = params["x_weird"]
    const_lambda = params["lambda"]
    n_0 = params["n_0"]
    a_n = params["a_n"]
    l_0 = params["l_0"]
    a_l = params["a_l"]

    Ir = P / (math.pi * a ** 2)
    q = 2 * alpha / (c * L)
    g = k / c

    hr = R / I
    ht = T / K

    r = []
    for i in range(I + 1):
        r.append(i * hr)
    temp1 = np.zeros(I + 1)

    A = []
    B = []
    C = []
    temp = -4 * g * ht / hr ** 2
    A.append(temp)
    temp = 1 + 4 * g * ht / hr ** 2 + q * ht
    B.append(temp)
    C.append(0.0)
    for ind in range(1, I):
        temp = -g * ht * (2 * ind + 1) / (2 * ind * hr ** 2)
        A.append(temp)
        temp = 1 + 2 * g * ht / hr ** 2 + q * ht
        B.append(temp)
        temp = -g * ht * (2 * ind - 1) / (2 * ind * hr ** 2)
        C.append(temp)
    A.append(0.0)
    temp = 1 + 2 * g * ht / hr ** 2 + q * ht
    B.append(temp)
    temp = -2 * g * ht / hr ** 2
    C.append(temp)

    percent = 0
    for k in range(1, K + 1):
        D = []

        # считаем коэффициенты D
        temp = temp1[0]
        if 0 < a:
            temp += fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[0]) * ht * Ir
        D.append(temp)

        for ind in range(1, I):
            temp = temp1[ind]
            if ind * hr < a:
                temp += fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[ind]) * ht * Ir
            D.append(temp)
        temp = temp1[I]
        if R < a:
            temp += fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[I]) * ht * Ir
        D.append(temp)

        alphas = []
        betas = []

        # считаем коэффициенты alphas и betas
        temp = -A[0] / B[0]
        alphas.append(temp)
        temp = D[0] / B[0]
        betas.append(temp)
        for ind in range(1, I):
            temp = -A[ind] / (B[ind] + C[ind] * alphas[ind - 1])
            alphas.append(temp)
            temp = (D[ind] - C[ind] * betas[ind - 1]) / (B[ind] + C[ind] * alphas[ind - 1])
            betas.append(temp)

        # считаем значения
        temp1[I] = (D[I] - C[I] * betas[I - 1]) / (B[I] + C[I] * alphas[I - 1])
        for i in range(I - 1, -1, -1):
            temp1[i] = alphas[i] * temp1[i + 1] + betas[i]
        if int(k / K * 100) > percent:
            percent = int(k / K * 100)
            print(str(percent) + " %")

    return temp1[0]


def fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp_value):
    n = n_0 + temp_value * a_l
    l = l_0 * (1 + a_l * temp_value)
    k_v_ch = 2 * np.pi * n / const_lambda
    eps = k_v_ch * l
    return 4 * x_weird * k_v_ch * (1 + n ** 2) / (4 * n ** 2 + (1 - n ** 2) ** 2 * np.sin(eps) ** 2)


exact_solution = 11.459007533277603
params = {"R": 3, "T": 10, "P": 60, "a": 0.3, "alpha": 0.005, "c": 1.65, "L": 1, "k": 0.59,
          "x_weird": 8.5 * 10e-6, "lambda": 10.6 * 10e-4, "n_0": 4, "a_n": 4 * 10e-4, "l_0": 1, "a_l": 4 * 10e-6,
          "I": 20, "K": 500}


value_time = time.time()
sol = func()
value_time = time.time() - value_time
print("sol = " + str(sol))
print("eps  = " + str(exact_solution - sol))
print("time = " + str(value_time))
print("I    = " + str(params["I"]))
print("K    = " + str(params["K"]))
