import math
import tkinter.messagebox as mb
import numpy as np


def explicit_U_t(params, status_bar):
    fi = params["fi"]
    R = params["R"]
    T = params["T"]
    P = params["P"]
    a = params["a"]
    alpha = params["alpha"]
    c = params["c"]
    L = params["L"]
    k = params["k"]
    I = params["I"]
    K = params["K"]
    r = params["r"]

    Ir = P / (math.pi * a ** 2)
    q = 2 * alpha / (c * L)
    g = k / c

    hr = R / I
    ht = T / K

    status_bar.config(text="Считаем по явной схеме.")
    status_bar.update()

    t = []
    for i in range(0, K+1):
        t.append(i * ht)

    U = [0.0]
    temp1 = np.zeros(I+1)

    number = int(r / hr)

    percent = 0
    for i in range(1, K+1):
        temp2 = np.zeros(I+1)
        temp2[0] = 4 * g * ht * (temp1[1] - temp1[0]) / (hr ** 2) + (1 - q * ht) * temp1[0] + ht * fi * Ir

        if temp2[0] < 0:
            mb.showerror(message="неустойчива")
            return None, None, None, None, None

        for j in range(1, I):
            temp = g * ht * (
                    (temp1[j + 1] - 2 * temp1[j] + temp1[j - 1]) / (hr ** 2) + (temp1[j + 1] - temp1[j - 1]) /
                    (2 * j * hr ** 2)) + (1 - q * ht) * temp1[j]
            if j * hr < a:
                temp += ht * fi * Ir
            temp2[j] = temp

        temp = 2 * g * (temp1[len(temp1) - 2] - temp1[len(temp1) - 1]) * ht / (hr ** 2) + (1 - q * ht) * temp1[
            len(temp1) - 1]

        temp2[I] = temp
        temp1 = temp2

        U.append(temp1[number])

        if int(i / K * 100) > percent:
            percent = int(i / K * 100)
            status_bar.config(text="Считаем по явной схеме." + str(percent) + "%")
            status_bar.update()

    status_bar.config(text="Построен график, полученный с помощью явной схемы.")
    status_bar.update()
    return "t, время", "U, температура", "явная: r = " + str(hr * number), t, U


def explicit_U_r(params, status_bar):
    fi = params["fi"]
    R = params["R"]
    T = params["T"]
    P = params["P"]
    a = params["a"]
    alpha = params["alpha"]
    c = params["c"]
    L = params["L"]
    k = params["k"]
    I = params["I"]
    K = params["K"]

    q = 2 * alpha / (c * L)
    g = k / c
    Ir = P / (math.pi * a ** 2)

    hr = R / I
    ht = T / K

    status_bar.config(text="Получен график аналитического решения.")
    status_bar.update()

    r = np.linspace(0, R, I+1)
    temp1 = np.zeros(I+1)

    percent = 0
    for i in range(1, K+1):
        temp2 = np.zeros(I+1)

        temp = 4 * g * ht * (temp1[1] - temp1[0]) / (hr ** 2) + (1 - q * ht) * temp1[0] + ht * fi * Ir
        temp2[0] = temp

        if temp2[0] < 0:
            mb.showerror(message="неустойчива")
            return None, None, None, None, None

        for j in range(1, I):
            temp = g * ht * ((temp1[j + 1] - 2 * temp1[j] + temp1[j - 1]) / (hr ** 2) +
                             (temp1[j + 1] - temp1[j - 1]) / (2 * j * hr ** 2)) + (1 - q * ht) * temp1[j]
            if j * hr < a:
                temp += ht * fi * Ir
            temp2[j] = temp

        temp = 2 * g * (temp1[I - 1] - temp1[I]) * ht / (hr ** 2) + (1 - q * ht) * temp1[I]
        temp2[I] = temp

        temp1 = temp2

        if int(i / K * 100) > percent:
            percent = int(i / K * 100)
            status_bar.config(text="Считаем по явной схеме." + str(percent) + "%")
            status_bar.update()

    status_bar.config(text="Построен график, полученный с помощью явной схемы.")
    status_bar.update()
    return "r, радиус", "U, температура", "явная: T = " + str(T), r, temp1
