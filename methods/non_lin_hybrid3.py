import math
import tkinter.messagebox as mb
import numpy as np


def fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp_value):
    n = n_0 + temp_value * a_l
    l = l_0 * (1 + a_l * temp_value)
    k_v_ch = 2 * np.pi * n / const_lambda
    eps = k_v_ch * l
    return 4 * x_weird * k_v_ch * (1 + n ** 2) / (4 * n ** 2 + (1 - n ** 2) ** 2 * np.sin(eps) ** 2)


def hybrid_U_t(params, status_bar):
    R = params["R"]
    T = params["T"]
    P = params["P"]
    a = params["a"]
    alpha = params["alpha"]
    c = params["c"]
    L = params["L"]
    k = params["k"]
    I = params["I"]
    K = params["K"]
    r = params["r"]
    x_weird = params["x_weird"]
    const_lambda = params["lambda"]
    n_0 = params["n_0"]
    a_n = params["a_n"]
    l_0 = params["l_0"]
    a_l = params["a_l"]

    Ir = P / (math.pi * a ** 2)
    q = 2 * alpha / (c * L)
    g = k / c

    hr = R / I
    ht = T / K

    status_bar.config(text="Считаем по 3-й гибридной схеме.")
    status_bar.update()

    if I % 2 != 0:
        mb.showerror(message="Кол-во узлов по r должно быть четным")
        return None, None, None, None, None

    t = []
    for i in range(0, K + 1):
        t.append(i * ht)

    temp1 = np.zeros(I + 1)
    U = [0.0]

    number = int(r / hr)

    percent = 0
    for i in range(1, K + 1):
        temp2 = np.zeros(I + 1)

        if i % 2 == 0:
            # вычисляем 0-й узел по r
            temp = 4 * g * ht * (temp1[1] - temp1[0]) / (hr ** 2) + (1 - q * ht) * temp1[0]
            temp2[0] = temp + ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[0]) * Ir

            # вычисляем последний узел по r
            temp = 2 * g * ht * (temp1[I - 1] - temp1[I]) / (hr ** 2) + (1 - q * ht) * temp1[I]
            temp2[I] = temp

            # считаем узлы с помощью явной схемы
            for j in range(2, I - 1, 2):
                temp = g * ht * ((temp1[j + 1] - 2 * temp1[j] + temp1[j - 1]) / (hr ** 2) +
                                 (temp1[j + 1] - temp1[j - 1]) / (2 * j * hr ** 2)) + (1 - q * ht) * temp1[j]
                if j * hr < a:
                    temp += ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[j]) * Ir
                temp2[j] = temp

                if temp < 0:
                    mb.showerror(message="неустойчива")
                    return None, None, None, None, None

            # считаем узлы с помощью неявной схемы
            for j in range(1, I, 2):
                temp = g * ht * ((temp2[j + 1] + temp2[j - 1]) / hr ** 2 +
                                 (temp2[j + 1] - temp2[j - 1]) / (2 * j * hr ** 2)) + temp1[j]
                if j * hr < a:
                    temp += ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[j]) * Ir
                temp /= 1 + 2 * g * ht / hr ** 2 + q * ht
                temp2[j] = temp

                if temp < 0:
                    mb.showerror(message="неустойчива")
                    return None, None, None, None, None

        else:
            # считаем узлы с помощью явной схемы
            for j in range(1, I, 2):
                temp = g * ht * ((temp1[j + 1] - 2 * temp1[j] + temp1[j - 1]) / (hr ** 2) +
                                 (temp1[j + 1] - temp1[j - 1]) / (2 * j * hr ** 2)) + (1 - q * ht) * temp1[j]
                if j * hr < a:
                    temp += ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[j]) * Ir
                temp2[j] = temp

                if temp < 0:
                    mb.showerror(message="неустойчива")
                    return None, None, None, None, None

            # вычисляем 0-й узел неявно
            temp = 4 * g * ht * temp2[1] / hr**2 + \
                   ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[0]) * Ir + temp1[0]
            temp /= 1 + 4 * g * ht / hr**2 + q * ht
            temp2[0] = temp

            # вычисляем последний узел неявно
            temp = 2 * g * ht * temp2[I-1] / hr**2 + temp1[I]
            temp /= 1 + 2 * g * ht / hr**2 + q * ht
            temp2[I] = temp

            # считаем узлы с помощью неявной схемы
            for j in range(2, I - 1, 2):
                temp = g * ht * ((temp2[j + 1] + temp2[j - 1]) / hr ** 2 +
                                 (temp2[j + 1] - temp2[j - 1]) / (2 * j * hr ** 2)) + temp1[j]
                if j * hr < a:
                    temp += ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[j]) * Ir
                temp /= 1 + 2 * g * ht / hr ** 2 + q * ht
                temp2[j] = temp

                if temp < 0:
                    mb.showerror(message="неустойчива")
                    return None, None, None, None, None

        U.append(temp2[number])
        temp1 = temp2
        if int(i / K * 100) > percent:
            percent = int(i / K * 100)
            status_bar.config(text="Считаем по 3-й гибридной схеме " + str(percent) + "%")
            status_bar.update()

    status_bar.config(text="Построен график, полученный с помощью 3-й гибридной схемы")
    status_bar.update()
    return "t, время", "U, температура", "Нелин. Гибрид. 3: r = " + str(hr * number), t, U


def hybrid_U_r(params, status_bar):
    R = params["R"]
    T = params["T"]
    P = params["P"]
    a = params["a"]
    alpha = params["alpha"]
    c = params["c"]
    L = params["L"]
    k = params["k"]
    I = params["I"]
    K = params["K"]
    x_weird = params["x_weird"]
    const_lambda = params["lambda"]
    n_0 = params["n_0"]
    a_n = params["a_n"]
    l_0 = params["l_0"]
    a_l = params["a_l"]

    q = 2 * alpha / (c * L)
    g = k / c
    Ir = P / (math.pi * a ** 2)

    hr = R / I
    ht = T / K

    status_bar.config(text="Считаем по 3-й гибридной схеме")
    status_bar.update()

    if I % 2 != 0:
        mb.showerror(message="Кол-во узлов по r должно быть четным")
        return None, None, None, None, None

    r = []
    for i in range(I + 1):
        r.append(i * hr)
    temp1 = np.zeros(I + 1)

    percent = 0
    for i in range(1, K + 1):
        temp2 = np.zeros(I + 1)

        if i % 2 == 0:
            # вычисляем 0-й узел по r
            temp = 4 * g * ht * (temp1[1] - temp1[0]) / (hr ** 2) + (1 - q * ht) * temp1[0] + \
                   ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[0]) * Ir
            temp2[0] = temp

            # вычисляем последний узел по r
            temp = 2 * g * ht * (temp1[I - 1] - temp1[I]) / (hr ** 2) + (1 - q * ht) * temp1[I]
            temp2[I] = temp

            # считаем узлы с помощью явной схемы
            for j in range(2, I - 1, 2):
                temp = g * ht * ((temp1[j + 1] - 2 * temp1[j] + temp1[j - 1]) / (hr ** 2) +
                                 (temp1[j + 1] - temp1[j - 1]) / (2 * j * hr ** 2)) + (1 - q * ht) * temp1[j]
                if j * hr < a:
                    temp += ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[j]) * Ir
                temp2[j] = temp

                if temp < 0:
                    mb.showerror(message="неустойчива")
                    return None, None, None, None, None

            # считаем узлы с помощью неявной схемы
            for j in range(1, I, 2):
                temp = g * ht * ((temp2[j + 1] + temp2[j - 1]) / hr ** 2 +
                                 (temp2[j + 1] - temp2[j - 1]) / (2 * j * hr ** 2)) + temp1[j]
                if j * hr < a:
                    temp += ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[j]) * Ir
                temp /= 1 + 2 * g * ht / hr ** 2 + q * ht
                temp2[j] = temp

                if temp < 0:
                    mb.showerror(message="неустойчива")
                    return None, None, None, None, None

        else:
            # считаем узлы с помощью явной схемы
            for j in range(1, I, 2):
                temp = g * ht * ((temp1[j + 1] - 2 * temp1[j] + temp1[j - 1]) / (hr ** 2) +
                                 (temp1[j + 1] - temp1[j - 1]) / (2 * j * hr ** 2)) + (1 - q * ht) * temp1[j]
                if j * hr < a:
                    temp += ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[j]) * Ir
                temp2[j] = temp

                if temp < 0:
                    mb.showerror(message="неустойчива")
                    return None, None, None, None, None

            # вычисляем 0-й узел неявно
            temp = 4 * g * ht * temp2[1] / hr ** 2 + \
                   ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[0]) * Ir + temp1[0]
            temp /= 1 + 4 * g * ht / hr ** 2 + q * ht
            temp2[0] = temp

            # вычисляем последний узел неявно
            temp = 2 * g * ht * temp2[I - 1] / hr ** 2 + temp1[I]
            temp /= 1 + 2 * g * ht / hr ** 2 + q * ht
            temp2[I] = temp

            # считаем узлы с помощью неявной схемы
            for j in range(2, I - 1, 2):
                temp = g * ht * ((temp2[j + 1] + temp2[j - 1]) / hr ** 2 +
                                 (temp2[j + 1] - temp2[j - 1]) / (2 * j * hr ** 2)) + temp1[j]
                if j * hr < a:
                    temp += ht * fi(x_weird, const_lambda, n_0, a_n, l_0, a_l, temp1[j]) * Ir
                temp /= 1 + 2 * g * ht / hr ** 2 + q * ht
                temp2[j] = temp

                if temp < 0:
                    mb.showerror(message="неустойчива")
                    return None, None, None, None, None

        temp1 = temp2
        if int(i / K * 100) > percent:
            percent = int(i / K * 100)
            status_bar.config(text="Считаем по 3-й гибридной схеме " + str(percent) + "%")
            status_bar.update()

    status_bar.config(text="Построен график, полученный с помощью 3-й гибридной схемы.")
    status_bar.update()
    return "r, радиус", "U, температура", "Нелин. Гибрид. 3: T = " + str(T), r, temp1
