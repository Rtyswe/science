import math
import numpy as np


def implicit_U_t(params, status_bar):
    fi = params["fi"]
    R = params["R"]
    T = params["T"]
    P = params["P"]
    a = params["a"]
    alpha = params["alpha"]
    c = params["c"]
    L = params["L"]
    k = params["k"]
    I = params["I"]
    K = params["K"]
    r = params["r"]

    Ir = P / (math.pi * a ** 2)
    q = 2 * alpha / (c * L)
    g = k / c

    hr = R / I
    ht = T / K

    status_bar.config(text="Считаем по неявной схеме.")
    status_bar.update()

    temp1 = np.zeros(I + 1)
    U = [0.0]
    t = []
    for i in range(K+1):
        t.append(i*ht)

    number = int(r / hr)

    A = []
    B = []
    C = []
    temp = -4 * g * ht / hr ** 2
    A.append(temp)
    temp = 1 + 4 * g * ht / hr ** 2 + q * ht
    B.append(temp)
    C.append(0.0)
    for ind in range(1, I):
        temp = -g * ht * (2 * ind + 1) / (2 * ind * hr ** 2)
        A.append(temp)
        temp = 1 + 2 * g * ht / hr ** 2 + q * ht
        B.append(temp)
        temp = -g * ht * (2 * ind - 1) / (2 * ind * hr ** 2)
        C.append(temp)
    A.append(0.0)
    temp = 1 + 2 * g * ht / hr ** 2 + q * ht
    B.append(temp)
    temp = -2 * g * ht / hr ** 2
    C.append(temp)

    percent = 0
    for k in range(1, K + 1):
        D = []

        # считаем коэффициенты D
        temp = temp1[0]
        if 0 < a:
            temp += fi * ht * Ir
        D.append(temp)

        for ind in range(1, I):
            temp = temp1[ind]
            if ind * hr < a:
                temp += fi * ht * Ir
            D.append(temp)
        temp = temp1[I]
        if R < a:
            temp += fi * ht * Ir
        D.append(temp)

        alphas = []
        betas = []

        # считаем коэффициенты alphas и betas
        temp = -A[0] / B[0]
        alphas.append(temp)
        temp = D[0] / B[0]
        betas.append(temp)
        for ind in range(1, I):
            temp = -A[ind] / (B[ind] + C[ind] * alphas[ind - 1])
            alphas.append(temp)
            temp = (D[ind] - C[ind] * betas[ind - 1]) / (B[ind] + C[ind] * alphas[ind - 1])
            betas.append(temp)

        # считаем значения
        temp1[I] = (D[I] - C[I] * betas[I - 1]) / (B[I] + C[I] * alphas[I - 1])
        for i in range(I - 1, -1, -1):
            temp1[i] = alphas[i] * temp1[i + 1] + betas[i]
        U.append(temp1[number])

        if int(k / K * 100) > percent:
            percent = int(k / K * 100)
            status_bar.config(text=("Считаем по неявной схеме." + str(percent) + "%"))
            status_bar.update()

    status_bar.config(text="Построен график, полученный с помощью неявной схемы.")
    status_bar.update()
    return "t, время", "U, температура", "r = " + str(r), t, U


def implicit_U_r(params, status_bar):
    fi = params["fi"]
    R = params["R"]
    T = params["T"]
    P = params["P"]
    a = params["a"]
    alpha = params["alpha"]
    c = params["c"]
    L = params["L"]
    k = params["k"]
    I = params["I"]
    K = params["K"]

    Ir = P / (math.pi * a ** 2)
    q = 2 * alpha / (c * L)
    g = k / c

    hr = R / I
    ht = T / K

    status_bar.config(text="Считаем по неявной схеме.")
    status_bar.update()

    r = []
    for i in range(I + 1):
        r.append(i * hr)
    temp1 = np.zeros(I + 1)

    A = []
    B = []
    C = []
    temp = -4 * g * ht / hr ** 2
    A.append(temp)
    temp = 1 + 4 * g * ht / hr ** 2 + q * ht
    B.append(temp)
    C.append(0.0)
    for ind in range(1, I):
        temp = -g * ht * (2 * ind + 1) / (2 * ind * hr ** 2)
        A.append(temp)
        temp = 1 + 2 * g * ht / hr ** 2 + q * ht
        B.append(temp)
        temp = -g * ht * (2 * ind - 1) / (2 * ind * hr ** 2)
        C.append(temp)
    A.append(0.0)
    temp = 1 + 2 * g * ht / hr ** 2 + q * ht
    B.append(temp)
    temp = -2 * g * ht / hr ** 2
    C.append(temp)

    percent = 0
    for k in range(1, K + 1):
        D = []

        # считаем коэффициенты D
        temp = temp1[0]
        if 0 < a:
            temp += fi * ht * Ir
        D.append(temp)

        for ind in range(1, I):
            temp = temp1[ind]
            if ind * hr < a:
                temp += fi * ht * Ir
            D.append(temp)
        temp = temp1[I]
        if R < a:
            temp += fi * ht * Ir
        D.append(temp)

        alphas = []
        betas = []

        # считаем коэффициенты alphas и betas
        temp = -A[0] / B[0]
        alphas.append(temp)
        temp = D[0] / B[0]
        betas.append(temp)
        for ind in range(1, I):
            temp = -A[ind] / (B[ind] + C[ind] * alphas[ind - 1])
            alphas.append(temp)
            temp = (D[ind] - C[ind] * betas[ind - 1]) / (B[ind] + C[ind] * alphas[ind - 1])
            betas.append(temp)

        # считаем значения
        temp1[I] = (D[I] - C[I] * betas[I - 1]) / (B[I] + C[I] * alphas[I - 1])
        for i in range(I - 1, -1, -1):
            temp1[i] = alphas[i] * temp1[i + 1] + betas[i]

        if int(k / K * 100) > percent:
            percent = int(k / K * 100)
            status_bar.config(text="Считаем по неявной схеме." + str(percent) + "%")
            status_bar.update()

    status_bar.config(text="Построен график, полученный с помощью неявной схемы.")
    status_bar.update()
    return "r, радиус", "U, температура", "T = " + str(T), r, temp1
