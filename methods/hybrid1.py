import math
import tkinter.messagebox as mb
import numpy as np


def search_node_number(radius, I, hr):
    for i in range(I + 1):
        if i * hr - hr / 2 < radius < i * hr + hr / 2:
            return i


def hybrid_U_t(params, status_bar):
    fi = params["fi"]
    R = params["R"]
    T = params["T"]
    P = params["P"]
    a = params["a"]
    alpha = params["alpha"]
    c = params["c"]
    L = params["L"]
    k = params["k"]
    I = params["I"]
    K = params["K"]
    r = params["r"]

    Ir = P / (math.pi * a ** 2)
    q = 2 * alpha / (c * L)
    g = k / c

    hr = R / I
    ht = T / K

    status_bar.config(text="Считаем по 1-й гибридной схеме.")
    status_bar.update()

    if I % 2 != 0:
        mb.showerror(message="Кол-во узлов по r должно быть четным")
        return

    t = []
    for i in range(0, K + 1):
        t.append(i * ht)

    temp1 = np.zeros(I + 1)
    U = [0.0]

    number = search_node_number(r, I, hr)

    percent = 0
    for i in range(1, K + 1):
        temp2 = np.zeros(I + 1)

        # вычисляем 0-й узел по r
        temp = 4 * g * ht * (temp1[1] - temp1[0]) / (hr ** 2) + (1 - q * ht) * temp1[0] + ht * fi * Ir
        temp2[0] = temp
        if number == 0:
            U.append(temp)

        # вычисляем последний узел по r
        temp = 2 * g * ht * (temp1[I - 1] - temp1[I]) / (hr ** 2) + (1 - q * ht) * temp1[I]
        temp2[I] = temp

        if number == I:
            U.append(temp)

        # считаем узлы с помощью явной схемы
        for j in range(2, I - 1, 2):
            temp = g * ht * ((temp1[j + 1] - 2 * temp1[j] + temp1[j - 1]) / (hr ** 2) +
                             (temp1[j + 1] - temp1[j - 1]) / (2 * j * hr ** 2)) + (1 - q * ht) * temp1[j]
            if j * hr < a:
                temp += ht * fi * Ir
            temp2[j] = temp

            if j == number:
                U.append(temp)

            if temp < 0:
                mb.showerror(message="неустойчива")
                return None, None, None, None, None

        # считаем узлы с помощью неявной схемы
        for j in range(1, I, 2):
            temp = g * ht * ((temp2[j + 1] + temp2[j - 1]) / hr ** 2 +
                             (temp2[j + 1] - temp2[j - 1]) / (2 * j * hr ** 2)) + temp1[j]
            if j * hr < a:
                temp += ht * fi * Ir
            temp /= 1 + 2 * g * ht / hr ** 2 + q * ht
            temp2[j] = temp

            if j == number:
                U.append(temp)

            if temp < 0:
                mb.showerror(message="неустойчива")
                return None, None, None, None, None

        temp1 = temp2
        if int(i / K * 100) > percent:
            percent = int(i / K * 100)
            status_bar.config(text="Считаем по 1-й гибридной схеме." + str(percent) + "%")
            status_bar.update()

    status_bar.config(text="Построен график, полученный с помощью 1-й гибридной схемы.")
    status_bar.update()
    return "t, время", "U, температура", "ГБ-1: r = " + str(hr * number), t, U


def hybrid_U_r(params, status_bar):
    fi = params["fi"]
    R = params["R"]
    T = params["T"]
    P = params["P"]
    a = params["a"]
    alpha = params["alpha"]
    c = params["c"]
    L = params["L"]
    k = params["k"]
    I = params["I"]
    K = params["K"]

    q = 2 * alpha / (c * L)
    g = k / c
    Ir = P / (math.pi * a ** 2)

    hr = R / I
    ht = T / K

    status_bar.config(text="Считаем по 1-й гибридной схеме.")
    status_bar.update()

    if I % 2 != 0:
        mb.showerror(message="Кол-во узлов по r должно быть четным")
        return None, None, None, None, None

    r = []
    for i in range(I + 1):
        r.append(i * hr)
    temp1 = np.zeros(I + 1)

    percent = 0
    for i in range(1, K + 1):
        temp2 = np.zeros(I + 1)

        # вычисляем 0-й узел по r
        temp = 4 * g * ht * (temp1[1] - temp1[0]) / (hr ** 2) + (1 - q * ht) * temp1[0] + ht * fi * Ir
        temp2[0] = temp

        # вычисляем последний узел по r
        temp = 2 * g * (temp1[I - 1] - temp1[I]) * ht / (hr ** 2) + (1 - q * ht) * temp1[I]
        temp2[I] = temp

        # считаем узлы с помощью явной схемы
        for j in range(2, I - 1, 2):
            temp = g * ht * ((temp1[j + 1] - 2 * temp1[j] + temp1[j - 1]) / (hr ** 2) +
                             (temp1[j + 1] - temp1[j - 1]) / (2 * j * hr ** 2)) + (1 - q * ht) * temp1[j]
            if j * hr < a:
                temp += ht * fi * Ir
            temp2[j] = temp

            if temp < 0:
                mb.showerror(message="неустойчива")
                return None, None, None, None, None

        # считаем узлы с помощью неявной схемы
        for j in range(1, I, 2):
            temp = g * ht * ((temp2[j + 1] + temp2[j - 1]) / hr ** 2 +
                             (temp2[j + 1] - temp2[j - 1]) / (2 * j * hr ** 2)) + temp1[j]
            if j * hr < a:
                temp += ht * fi * Ir
            temp /= 1 + 2 * g * ht / hr ** 2 + q * ht
            temp2[j] = temp

            if temp < 0:
                mb.showerror(message="неустойчива")
                return None, None, None, None, None

        temp1 = temp2
        if int(i / K * 100) > percent:
            percent = int(i / K * 100)
            status_bar.config(text="Считаем по 1-й гибридной схеме." + str(percent) + "%")
            status_bar.update()

    status_bar.config(text="Построен график, полученный с помощью 1-й гибридной схемы.")
    status_bar.update()
    return "r, радиус", "U, температура", "ГБ-1: T = " + str(T), r, temp1
