import math
from scipy.special import *
from scipy.integrate import quad


def f(x, nu, a, R):
    return math.exp(-(x / a) ** 2) * j0(nu * x / R) * x


def Fn(nu, fi, P, a, R):
    v, err = quad(f, 0, 1, args=(nu, a, R))
    return 2 * fi * v * P / ((R * j0(nu) * a) ** 2 * math.pi)


def Lambda(nu, R, g, q):
    return ((nu / R) ** 2) * g + q


def Temperature(time, radius, q, nuN, R, fi, P, g, a):
    amount = 0
    for nu in nuN:
        lambd = Lambda(nu, R, g, q)
        fn = Fn(nu, fi, P, a, R)
        amount += fn * (1 - math.exp(-lambd * time)) * j0(nu * radius / R) / lambd
    return amount


def anal_U_t():
    fi = 0.1
    R = 3
    T = 10
    P = 60
    a = 0.3
    alpha = 0.005
    c = 1.65
    L = 1
    k = 0.59
    q = 2 * alpha / (c * L)
    g = k / c

    with open("k.txt") as file:
        nuN = [float(row.strip()) for row in file]

    return Temperature(T, 0, q, nuN, R, fi, P, g, a)


print(anal_U_t())
