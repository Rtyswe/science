import numpy as np


def ksi(arg):
    return 8 * np.exp(-(arg / (0.1 * R))**2)


def U_r_nonexplicit():

    hr = R / I
    ht = T / K

    r_arr = []
    temp1 = []
    for i in range(I + 1):
        r_arr.append(i * hr)
        temp1.append(ksi(r_arr[i]))

    t_arr = []
    for t in range(K + 1):
        t_arr.append(t * ht)


    percent = 0
    A = []
    B = []
    C = []
    A.append(-4 * const_K / hr ** 2)
    B.append(c / ht + 4 * const_K / hr ** 2)
    C.append(0.0)
    for ind in range(1, I):
        A.append(-const_K / hr ** 2 - const_K / (hr * r_arr[ind]))
        B.append(c / ht + 2 * const_K / hr ** 2)
        C.append(-const_K / hr ** 2 + const_K / (hr * r_arr[ind]))
    A.append(0.0)
    B.append(c / ht + 2 * const_K / hr ** 2)
    C.append(-2 * const_K / hr ** 2)

    for k in range(1, K + 1):
        D = []
        # считаем коэффициенты A, B, C и D
        for ind in range(0, I + 1):
            D.append(temp1[ind] * c / ht)

        alphas = []
        betas = []

        # считаем коэффициенты alphas и betas
        alphas.append(-A[0] / B[0])
        betas.append(D[0] / B[0])
        for ind in range(1, I):
            alphas.append(-A[ind] / (B[ind] + C[ind] * alphas[ind - 1]))
            betas.append((D[ind] - C[ind] * betas[ind - 1]) / (B[ind] + C[ind] * alphas[ind - 1]))

        # считаем значения
        temp1[I] = (D[I] - C[I] * betas[I - 1]) / (B[I] + alphas[I - 1] * C[I])
        for i in range(I - 1, -1, -1):
            temp1[i] = alphas[i] * temp1[i + 1] + betas[i]

        # if int(k / K * 100) > percent:
        #     percent = int(k / K * 100)
        #     print("{}%".format(percent))

    return temp1[0]


const_K = 0.84
c = 0.65
R = 4
T = 10
r = 0.1

I = 6400
K = 400

print("I = ", I, end=" ")
print("K = ", K, end=" ")

U_ht_hr = U_r_nonexplicit()
I *= 2
U_2ht_hr = U_r_nonexplicit()
I *= 2
U_4ht_hr = U_r_nonexplicit()

print("E_ht_hr/2 = " + str(U_ht_hr - U_2ht_hr), end=" ")
print("E_ht_hr/4 = " + str(U_2ht_hr - U_4ht_hr), end=" ")
print("Del'ta = " + str((U_ht_hr - U_2ht_hr) / (U_2ht_hr - U_4ht_hr)), end=" ")
