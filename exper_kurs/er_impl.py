import math
import numpy as np


def implicit_U_t():
    hr = R / I
    ht = T / K

    temp1 = np.zeros(I + 1)
    t = []
    for i in range(K+1):
        t.append(i*ht)

    A = []
    B = []
    C = []
    temp = -4 * g * ht / hr ** 2
    A.append(temp)
    temp = 1 + 4 * g * ht / hr ** 2 + q * ht
    B.append(temp)
    C.append(0.0)
    for ind in range(1, I):
        temp = -g * ht * (2 * ind + 1) / (2 * ind * hr ** 2)
        A.append(temp)
        temp = 1 + 2 * g * ht / hr ** 2 + q * ht
        B.append(temp)
        temp = -g * ht * (2 * ind - 1) / (2 * ind * hr ** 2)
        C.append(temp)
    A.append(0.0)
    temp = 1 + 2 * g * ht / hr ** 2 + q * ht
    B.append(temp)
    temp = -2 * g * ht / hr ** 2
    C.append(temp)

    percent = 0
    for k in range(1, K + 1):
        D = []

        # считаем коэффициенты D
        for ind in range(I+1):
            temp = temp1[ind] # + fi * ht * Ir * math.exp(-(ind*hr/a)**2)
            if ind * hr < a:
                temp += fi * ht * Ir
            D.append(temp)

        alphas = []
        betas = []

        # считаем коэффициенты alphas и betas
        temp = -A[0] / B[0]
        alphas.append(temp)
        temp = D[0] / B[0]
        betas.append(temp)
        for ind in range(1, I):
            temp = -A[ind] / (B[ind] + C[ind] * alphas[ind - 1])
            alphas.append(temp)
            temp = (D[ind] - C[ind] * betas[ind - 1]) / (B[ind] + C[ind] * alphas[ind - 1])
            betas.append(temp)

        # считаем значения
        temp1[I] = (D[I] - C[I] * betas[I - 1]) / (B[I] + C[I] * alphas[I - 1])
        for i in range(I - 1, -1, -1):
            temp1[i] = alphas[i] * temp1[i + 1] + betas[i]

    return temp1[0]


fi = 0.1
R = 3
T = 10
P = 60
a = 0.3
alpha = 0.005
c = 1.65
L = 1
k = 0.59

q = 2 * alpha / (c * L)
g = k / c
Ir = P / (math.pi * a ** 2)

K = 800
I = 100

print("I = ", I, end=" ")
print("K = ", K, end=" ")

U_ht_hr = implicit_U_t()
K *= 2
U_2ht_hr = implicit_U_t()
K *= 2
U_4ht_hr = implicit_U_t()

print("E_ht/2_hr = " + str(U_ht_hr - U_2ht_hr), end=" ")
print("E_ht/4_hr = " + str(U_2ht_hr - U_4ht_hr), end=" ")
print("Del'ta = " + str((U_ht_hr - U_2ht_hr) / (U_2ht_hr - U_4ht_hr)), end=" ")
