from tkinter import *
from tkinter.ttk import Combobox

methods = ["Аналитический", "Явная схема", "Неявная схема", "1-я гибридная схема", "2-я гибридная схема",
           "3-я гибридная схема"]
non_lin_methods = ["Неявная схема", "3-я гибридная схема"]


def change_lin():
    global method_combo
    if cb.get() == 0:
        method_combo['values'] = methods
    elif cb.get() == 1:
        method_combo['values'] = non_lin_methods


root = Tk()

method_label = Label(root, text="Выберите метод")
method_combo = Combobox(root, values=methods)
method_combo.set("...")
method_combo['state'] = 'readonly'

method_label.grid(row=1, column=1, ipadx=2, ipady=2, padx=5, pady=5)
method_combo.grid(row=1, column=2, ipadx=2, ipady=2, padx=5, pady=5)

cb = IntVar()
Checkbutton(text="non_lin", variable=cb, onvalue=1, offvalue=0, command=change_lin).grid()

root.mainloop()
