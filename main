import sys
import tkinter as tk
import tkinter.messagebox as mb
from tkinter.ttk import Combobox
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import analitic
from methods import explicit, implicit, hybrid1, hybrid2, hybrid3, non_lin_implicit, non_lin_hybrid3, non_lin_explicit

lin_methods = ["Аналитический", "Явная схема", "Неявная схема",
               "1-я гибридная схема", "2-я гибридная схема", "3-я гибридная схема"]
non_lin_methods = ["Явная схема", "Неявная схема", "3-я гибридная схема"]
dependence = ["U от t", "U от r"]

dependence_old = ""
fi_old = R_old = P_old = a_old = alpha_old = c_old = L_old = k_old = 0
params = {}


def isNeedToClear():
    global fi_old, R_old, P_old, a_old, alpha_old, c_old, L_old, k_old, dependence_old
    is_need = False
    if fi_old != fi_entry.get():
        fi_old = fi_entry.get()
        is_need = True
    if R_old != R_entry.get():
        R_old = R_entry.get()
        is_need = True
    if P_old != P_entry.get():
        P_old = P_entry.get()
        is_need = True
    if a_old != a_entry.get():
        a_old = a_entry.get()
        is_need = True
    if alpha_old != alpha_entry.get():
        alpha_old = alpha_entry.get()
        is_need = True
    if c_old != c_entry.get():
        c_old = c_entry.get()
        is_need = True
    if L_old != L_entry.get():
        L_old = L_entry.get()
        is_need = True
    if dependence_old != dependence_combo.get():
        dependence_old = dependence_combo.get()
        is_need = True

    return is_need


def clear():
    print("чистимчистимчистим")
    plt.clf()
    plt.plot()
    fig.canvas.draw()


def disableEntry(entry):
    entry.config(state='disable')


def allowEntry(entry):
    entry.config(state='normal')


def on_closing_main():
    if mb.askokcancel("Выход из приложения", "Хотите выйти?"):
        sys.exit(0)


def methodChanged(event):
    if method_combo.get() == "Аналитический":
        disableEntry(I_entry)
        disableEntry(K_entry)
    else:
        allowEntry(I_entry)
        allowEntry(K_entry)


def dependenceChanged(event):
    if dependence_combo.get() == "U от r":
        disableEntry(r_entry)
    else:
        allowEntry(r_entry)


def change_methods():
    global method_combo
    if is_non_lin.get() == 0:
        method_combo['values'] = lin_methods
    elif is_non_lin.get() == 1:
        method_combo['values'] = non_lin_methods


def pack_params():
    params["fi"] = float(fi_entry.get())
    params["R"] = float(R_entry.get())
    params["T"] = float(T_entry.get())
    params["P"] = float(P_entry.get())
    params["a"] = float(a_entry.get())
    params["alpha"] = float(alpha_entry.get())
    params["c"] = float(c_entry.get())
    params["L"] = float(L_entry.get())
    params["k"] = float(k_entry.get())
    params["r"] = float(r_entry.get())
    params["I"] = int(I_entry.get())
    params["K"] = int(K_entry.get())
    params["x_weird"] = float(x_weird_entry.get()) * 10e-6
    params["lambda"] = float(lambda_entry.get()) * 10e-4
    params["n_0"] = float(n_0_entry.get())
    params["a_n"] = float(a_n_entry.get()) * 10e-4
    params["l_0"] = float(l_0_entry.get())
    params["a_l"] = float(a_l_entry.get()) * 10e-6


def buildGraph():
    if method_combo.get() == "...":
        status_bar.config(text="Выберите метод.")
        return
    elif dependence_combo.get() == "...":
        status_bar.config(text="Выберите зависимость.")
        return

    x_label = y_label = label = ""
    x = y = []

    pack_params()

    graph_btn.config(state='disable')
    if is_non_lin.get() == 0:
        if method_combo.get() == "Аналитический":
            if dependence_combo.get() == "U от r":
                x_label, y_label, label, x, y = analitic.anal_U_r(params, status_bar)
            elif dependence_combo.get() == "U от t":
                x_label, y_label, label, x, y = analitic.anal_U_t(params, status_bar)

        elif method_combo.get() == "Явная схема":
            if dependence_combo.get() == "U от r":
                x_label, y_label, label, x, y = explicit.explicit_U_r(params, status_bar)
            elif dependence_combo.get() == "U от t":
                x_label, y_label, label, x, y = explicit.explicit_U_t(params, status_bar)

        elif method_combo.get() == "Неявная схема":
            if dependence_combo.get() == "U от r":
                x_label, y_label, label, x, y = implicit.implicit_U_r(params, status_bar)
            elif dependence_combo.get() == "U от t":
                x_label, y_label, label, x, y = implicit.implicit_U_t(params, status_bar)

        elif method_combo.get() == "1-я гибридная схема":
            if dependence_combo.get() == "U от r":
                x_label, y_label, label, x, y = hybrid1.hybrid_U_r(params, status_bar)
            elif dependence_combo.get() == "U от t":
                x_label, y_label, label, x, y = hybrid1.hybrid_U_t(params, status_bar)

        elif method_combo.get() == "2-я гибридная схема":
            if dependence_combo.get() == "U от r":
                x_label, y_label, label, x, y = hybrid2.hybrid_U_r(params, status_bar)
            elif dependence_combo.get() == "U от t":
                x_label, y_label, label, x, y = hybrid2.hybrid_U_t(params, status_bar)

        elif method_combo.get() == "3-я гибридная схема":
            if dependence_combo.get() == "U от r":
                x_label, y_label, label, x, y = hybrid3.hybrid_U_r(params, status_bar)
            elif dependence_combo.get() == "U от t":
                x_label, y_label, label, x, y = hybrid3.hybrid_U_t(params, status_bar)

    elif is_non_lin.get() == 1:
        if method_combo.get() == "Явная схема":
            if dependence_combo.get() == "U от r":
                x_label, y_label, label, x, y = non_lin_explicit.explicit_U_r(params, status_bar)
            elif dependence_combo.get() == "U от t":
                x_label, y_label, label, x, y = non_lin_explicit.explicit_U_t(params, status_bar)

        elif method_combo.get() == "Неявная схема":
            if dependence_combo.get() == "U от r":
                x_label, y_label, label, x, y = non_lin_implicit.implicit_U_r(params, status_bar)
            elif dependence_combo.get() == "U от t":
                x_label, y_label, label, x, y = non_lin_implicit.implicit_U_t(params, status_bar)

        elif method_combo.get() == "3-я гибридная схема":
            if dependence_combo.get() == "U от r":
                x_label, y_label, label, x, y = non_lin_hybrid3.hybrid_U_r(params, status_bar)
            elif dependence_combo.get() == "U от t":
                x_label, y_label, label, x, y = non_lin_hybrid3.hybrid_U_t(params, status_bar)

    graph_btn.config(state='normal')
    if isNeedToClear():
        clear()
    if x_label is None:
        return
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.plot(x, y, label=label)
    plt.legend()

    fig.canvas.draw()


root = tk.Tk()
root.protocol("WM_DELETE_WINDOW", on_closing_main)
root.title('Graph')
root.resizable(width=False, height=False)

method_label = tk.Label(root, text="Выберите метод")
method_combo = Combobox(root, values=lin_methods)
method_combo.set("...")
method_combo['state'] = 'readonly'
method_combo.bind('<<ComboboxSelected>>', methodChanged)

method_label.grid(row=1, column=1, ipadx=2, ipady=2, padx=5, pady=5)
method_combo.grid(row=1, column=2, ipadx=2, ipady=2, padx=5, pady=5)

dependence_label = tk.Label(root, text="Выберите зависимость")
dependence_combo = Combobox(root, values=dependence)
dependence_combo.set("...")
dependence_combo['state'] = 'readonly'
dependence_combo.bind('<<ComboboxSelected>>', dependenceChanged)

dependence_label.grid(row=2, column=1, ipadx=2, ipady=2, padx=5, pady=5)
dependence_combo.grid(row=2, column=2, ipadx=2, ipady=2, padx=5, pady=5)

fi_label = tk.Label(root, text="fi")
fi_entry = tk.Entry(root)
fi_entry.insert(0, "0.1")

fi_label.grid(row=3, column=1, ipadx=5, ipady=6, padx=5, pady=5)
fi_entry.grid(row=3, column=2, ipadx=5, ipady=6, padx=5, pady=5)

# для R
R_label = tk.Label(root, text="R")
R_entry = tk.Entry(root)
R_entry.insert(0, "3")

R_label.grid(row=4, column=1, ipadx=5, ipady=6, padx=5, pady=5)
R_entry.grid(row=4, column=2, ipadx=5, ipady=6, padx=5, pady=5)

# для T
T_label = tk.Label(root, text="T")
T_entry = tk.Entry(root)
T_entry.insert(0, "10")

T_label.grid(row=5, column=1, ipadx=5, ipady=6, padx=5, pady=5)
T_entry.grid(row=5, column=2, ipadx=5, ipady=6, padx=5, pady=5)

# для P
P_label = tk.Label(root, text="P")
P_entry = tk.Entry(root)
P_entry.insert(0, "60")

P_label.grid(row=6, column=1, ipadx=5, ipady=6, padx=5, pady=5)
P_entry.grid(row=6, column=2, ipadx=5, ipady=6, padx=5, pady=5)

# для a
a_label = tk.Label(root, text="a")
a_entry = tk.Entry(root)
a_entry.insert(0, "0.3")

a_label.grid(row=7, column=1, ipadx=5, ipady=6, padx=5, pady=5)
a_entry.grid(row=7, column=2, ipadx=5, ipady=6, padx=5, pady=5)

# для alpha
alpha_label = tk.Label(root, text="alpha")
alpha_entry = tk.Entry(root)
alpha_entry.insert(0, "0.005")

alpha_label.grid(row=8, column=1, ipadx=5, ipady=6, padx=5, pady=5)
alpha_entry.grid(row=8, column=2, ipadx=5, ipady=6, padx=5, pady=5)

# для c
c_label = tk.Label(root, text="c")
c_entry = tk.Entry(root)
c_entry.insert(0, "1.65")

c_label.grid(row=9, column=1, ipadx=5, ipady=6, padx=5, pady=5)
c_entry.grid(row=9, column=2, ipadx=5, ipady=6, padx=5, pady=5)

# для L
L_label = tk.Label(root, text="L")
L_entry = tk.Entry(root)
L_entry.insert(0, "1")

L_label.grid(row=10, column=1, ipadx=5, ipady=6, padx=5, pady=5)
L_entry.grid(row=10, column=2, ipadx=5, ipady=6, padx=5, pady=5)

# для k
k_label = tk.Label(root, text="k")
k_entry = tk.Entry(root)
k_entry.insert(0, "0.59")

k_label.grid(row=11, column=1, ipadx=5, ipady=6, padx=5, pady=5)
k_entry.grid(row=11, column=2, ipadx=5, ipady=6, padx=5, pady=5)

r_label = tk.Label(root, text="r")
r_entry = tk.Entry(root)
r_entry.insert(0, "0.1")

r_label.grid(row=12, column=1, ipadx=5, ipady=6, padx=5, pady=5)
r_entry.grid(row=12, column=2, ipadx=5, ipady=6, padx=5, pady=5)

# кол-во узлов по радиусу
I_label = tk.Label(root, text="I")
I_entry = tk.Entry(root)
I_entry.insert(0, "50")

I_label.grid(row=13, column=1, ipadx=5, ipady=6, padx=5, pady=5)
I_entry.grid(row=13, column=2, ipadx=5, ipady=6, padx=5, pady=5)

# кол-во узлов по времени
K_label = tk.Label(root, text="K")
K_entry = tk.Entry(root)
K_entry.insert(0, "100000")

K_label.grid(row=14, column=1, ipadx=5, ipady=6, padx=5, pady=5)
K_entry.grid(row=14, column=2, ipadx=5, ipady=6, padx=5, pady=5)

graph_btn = tk.Button(root, text="Построить график", command=buildGraph)
graph_btn.grid(row=15, column=2, ipadx=5, ipady=6, padx=5, pady=5)

clear_btn = tk.Button(root, text="Очистить график", command=clear)
clear_btn.grid(row=15, column=1, ipadx=5, ipady=6, padx=5, pady=5)

plt.plot()
fig = plt.figure(1)
canvas = FigureCanvasTkAgg(fig, master=root)
plot_widget = canvas.get_tk_widget().grid(row=1, column=3, rowspan=14, padx=20)

status_bar = tk.Label(root, text="Status bar")
status_bar.grid(row=15, column=3)

# Параметры для нелинейной задачи
# Показатель поглащения
x_weird_label = tk.Label(root, text="Показатель поглащения, 10^(-6)")
x_weird_entry = tk.Entry(root)
x_weird_entry.insert(0, "8.5")

x_weird_label.grid(row=3, column=4, ipadx=5, ipady=6, padx=5, pady=5)
x_weird_entry.grid(row=3, column=5, ipadx=5, ipady=6, padx=5, pady=5)

# Длина волны
lambda_label = tk.Label(root, text="lambda, 10^(-4)")
lambda_entry = tk.Entry(root)
lambda_entry.insert(0, "10.6")

lambda_label.grid(row=4, column=4, ipadx=5, ipady=6, padx=5, pady=5)
lambda_entry.grid(row=4, column=5, ipadx=5, ipady=6, padx=5, pady=5)

# Коэффициент преломления до нагрева
n_0_label = tk.Label(root, text="n_0")
n_0_entry = tk.Entry(root)
n_0_entry.insert(0, "4")

n_0_label.grid(row=5, column=4, ipadx=5, ipady=6, padx=5, pady=5)
n_0_entry.grid(row=5, column=5, ipadx=5, ipady=6, padx=5, pady=5)

# Коэффициент теплового изменения показателя преломления
a_n_label = tk.Label(root, text="a_n, 10^(-4)")
a_n_entry = tk.Entry(root)
a_n_entry.insert(0, "4")

a_n_label.grid(row=6, column=4, ipadx=5, ipady=6, padx=5, pady=5)
a_n_entry.grid(row=6, column=5, ipadx=5, ipady=6, padx=5, pady=5)

# Толщина до нагрева
l_0_label = tk.Label(root, text="l_0")
l_0_entry = tk.Entry(root)
l_0_entry.insert(0, "1")

l_0_label.grid(row=7, column=4, ipadx=5, ipady=6, padx=5, pady=5)
l_0_entry.grid(row=7, column=5, ipadx=5, ipady=6, padx=5, pady=5)

# Коэффициент теплового изменения показателя преломления
a_l_label = tk.Label(root, text="a_l, 10^(-6)")
a_l_entry = tk.Entry(root)
a_l_entry.insert(0, "4")

a_l_label.grid(row=8, column=4, ipadx=5, ipady=6, padx=5, pady=5)
a_l_entry.grid(row=8, column=5, ipadx=5, ipady=6, padx=5, pady=5)

is_non_lin = tk.IntVar()
tk.Checkbutton(text="Нелинейность", variable=is_non_lin, onvalue=1, offvalue=0, command=change_methods)\
    .grid(row=1, column=4, columnspan=2, rowspan=2)

root.mainloop()
