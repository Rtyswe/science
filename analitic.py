import math
from scipy.special import *


def Fn(nu, fi, P, a, R):
    return 2 * fi * P * j1(nu * a / R) / (math.pi * a * nu * R * j0(nu) ** 2)


def F0(fi, P, R):
    return fi * P / R ** 2 / math.pi


def Lambda(nu, R, g, q):
    return ((nu / R) ** 2) * g + q


def Temperature(time, radius, q, nuN, R, fi, P, g, a):
    amount = F0(fi, P, R) * (1 - math.exp(-q * time)) / q
    for j in range(1, len(nuN)):
        lambd = Lambda(nuN[j], R, g, q)
        fn = Fn(nuN[j], fi, P, a, R)
        amount += fn * (1 - math.exp(-lambd * time)) * j0(nuN[j] * radius / R) / lambd
    return amount


def anal_U_t(params, status_bar):
    fi = params["fi"]
    R = params["R"]
    T = params["T"]
    P = params["P"]
    a = params["a"]
    alpha = params["alpha"]
    c = params["c"]
    L = params["L"]
    k = params["k"]
    r = params["r"]
    q = 2 * alpha / (c * L)
    g = k / c
    points = 500

    with open("k.txt") as file:
        nuN = [float(row.strip()) for row in file]

    t = []
    U = []

    for i in range(0, points + 1):
        t.append(T / points * i)
        U.append(Temperature(T / points * i, r, q, nuN, R, fi, P, g, a))

    status_bar.config(text="Получен график аналитического решения.")
    return "t, время", "U, температура", "Аналитик.: r = " + str(r), t, U


def anal_U_r(params, status_bar):
    fi = params["fi"]
    R = params["R"]
    T = params["T"]
    P = params["P"]
    a = params["a"]
    alpha = params["alpha"]
    c = params["c"]
    L = params["L"]
    k = params["k"]
    q = 2 * alpha / (c * L)
    g = k / c
    points = 1000

    with open("k.txt") as file:
        nuN = [float(row.strip()) for row in file]

    r = []
    U = []

    for i in range(0, points + 1):
        r.append(R / points * i)
        U.append(Temperature(T, R / points * i, q, nuN, R, fi, P, g, a))

    status_bar.config(text="Получен график аналитического решения.")
    return "r, радиус", "U, температура", "Аналитик.: t = " + str(T), r, U
