import math

fi = 0.1
R = 3
T = 1000
P = 60
a = R / 10
Ir = P / (math.pi * a ** 2)

alpha = 0.005
c = 1.65
L = 1
q = 2 * alpha / (c * L)

K = 0.59
g = K / c
fi = fi / c

hr = 0.1
ht = 0.001

t = []
for k in range(0, int(T/ht)+1):
    t.append(k*ht)
r = []
for i in range(0, int(R/hr)+1):
    r.append(i*hr)


t_points = [100.0, 200.0]
r_points = [0.0, 0.2, 1.0, 3.0]
temp1 = []
for i in range(int(R/hr)+1):
    temp1.append(0.0)

Ut = []
for i in range(len(r_points)):
    Ut.append([])
    Ut[i].append(0.0)
Ur = []
U = []

for i in range(int(R/hr)+1):
    temp1[i] = 0.0


for k in range(int(T/ht)):
    temp2 = []
    temp = 4 * g * ht * (temp1[1] - temp1[0]) / (hr ** 2) + (1 - q * ht) * temp1[0] + ht * fi * Ir
    temp2.append(temp)

    Ut[0].append(temp)

    for i in range(1, int(R/hr)):
        temp = g * ht * ((temp1[i+1] - 2*temp1[i] + temp1[i-1]) / (hr ** 2) + (temp1[i+1] - temp1[i-1]) / (2 * i * hr ** 2)) + (1 - q * ht) * temp1[i]
        if i*hr <= R/10:
            temp += ht * fi * Ir
        temp2.append(temp)
        if r_points.__contains__(i * hr):
            Ut[r_points.index(i * hr)].append(temp)

    temp = 4 * g * (temp1[len(temp1) - 2] - temp1[len(temp1) - 1]) * ht / (hr ** 2) + (1 - q * ht) * temp1[len(temp1) - 1]
    temp2.append(temp)
    Ut[len(Ut)-1].append(temp)
    temp1 = temp2

    if t_points.__contains__((k+1)*ht):
        Ur.append(temp1)
    print(round(k / (T/ht) * 100, 2))

