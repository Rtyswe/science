import matplotlib.pyplot as plt
import math
from scipy.special import *


def Fn(nu):
    return 2 * fi * P * j1(nu * a / R) / math.pi / a / nu


def F0():
    return fi * P / R / math.pi


def Lambda(nu):
    return ((nu / R) ** 2) * g + q


def Temperature(time, radius):
    amount = F0() * (1 - math.exp(-q * time)) / q
    for j in range(1, len(nuN)):
        amount += (1 - math.exp(-Lambda(nuN[j]) * time)) * j0(nuN[j] * radius / R) * Fn(nuN[j]) / Lambda(nuN[j])
    return amount


def frange(x, y, jump):
    while x <= y:
        yield x
        x += jump


def U_t():
    t = [1000.0]
    r = []
    for i in frange(0, R, 0.01):
        r.append(i)

    U = []
    for k in t:
        tempU = []
        for i in r:
            tempU.append(Temperature(k, i))
        U.append(tempU)

    plt.xlabel("r, радиус")
    plt.ylabel("U(t), температура")
    for i in range(0, len(U)):
        plt.plot(r, U[i], label="t = " + str(int(t[i])))
    plt.legend()
    plt.show()


def U_r():
    t = []
    for i in range(0, 1001):
        t.append(float(i))
    r = [0.0]
    U = []
    for k in r:
        tempU = []
        for i in t:
            tempU.append(Temperature(i, k))
        U.append(tempU)

    plt.xlabel("t, время")
    plt.ylabel("U(t), температура")
    for i in range(0, len(U)):
        plt.plot(t, U[i], label="r = " + str(r[i]))
    plt.legend()
    plt.show()


fi = 0.1
R = 3
P = 20
a = R / 10
Ir = P / (math.pi * a ** 2)
alpha = 0.005
c = 1.65
L = 1
q = 2 * alpha / (c * L)
K = 0.11
g = K / c
fi = fi / c

with open("../k.txt") as file:
    nuN = [float(row.strip()) for row in file]
U_r()
U_t()
